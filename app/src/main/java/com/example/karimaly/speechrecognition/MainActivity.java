package com.example.karimaly.speechrecognition;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

	private static final int RESULT_SPEECH = 1;

	Button b1;
	ListView lv;
	Toast toast;

	float[] confidenceLevel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		lv = (ListView) findViewById(R.id.listView1);
		b1 = (Button) findViewById(R.id.button1);

		toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);

		b1.setOnClickListener(this);
		lv.setOnItemClickListener(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RESULT_SPEECH && resultCode == RESULT_OK && data != null) {
			ArrayList<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
			if(results != null) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
					confidenceLevel = data.getFloatArrayExtra(RecognizerIntent.EXTRA_CONFIDENCE_SCORES);
				}
				lv.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, results));
			}
		}
	}

	@Override
	public void onClick(View view) {
		Intent voiceRecognize = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		voiceRecognize
				.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
				.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getClass().getPackage().getName())
				.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ar-EG")
				.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak now please")
				.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS, 2000)
				.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, 3000)
				.putExtra("android.speech.extra.EXTRA_ADDITIONAL_LANGUAGES", new String[]{});

		if(voiceRecognize.resolveActivity(getPackageManager()) != null) {
			Log.d("VOICE", "App package: " + voiceRecognize.resolveActivity(getPackageManager()).getPackageName());
			
			startActivityForResult(voiceRecognize, RESULT_SPEECH);
		} else {
			toast.setText("There is no recognizer installed on the device\nPlease install ");
			toast.show();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
		if(confidenceLevel != null) {
			if(confidenceLevel[i] != -1)
				toast.setText("Confidence: " + (confidenceLevel[i] * 100) + "%");
			else
				toast.setText("Confidence: Unavailable");

			toast.show();
		}
	}
}
